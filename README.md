**Discontinued** experiment, I started it back in 2019 and there is now much more solid, advanced and open-source tools to achieve similar purpose (container powered network simulation).

To cite a few, I recommend you to have a look at: 
- https://github.com/ipspace/netsim-tools
- https://containerlab.srlinux.dev/
- https://github.com/networkop/k8s-topo




# myvlabmanager - vagrant lab manager

My own basic virtual/vagrant lab manager.
Simple script to create and manage network lab topologies with Vagrant.

-- Still under construction ---

## Depedencies

### System packages
The following system packages are require to make this work.
```
vagrant python3 python3-pip virtinst libvirt-bin libvirt-dev qemu-utils qemu virtualbox socat screen nginx
```
You need a working installation of Vagrant, version should be 2.0.3 or above, if needed here's a procedure to get the desired version:
```
wget -c https://releases.hashicorp.com/vagrant/2.0.3/vagrant_2.0.3_x86_64.deb
sudo dpkg -i vagrant_2.0.3_x86_64.deb
vagrant plugin install vagrant-libvirt
vagrant plugin install vagrant-mutate
sudo /etc/init.d/libvirt-bin restart
sudo addgroup libvirtd
sudo usermod -a -G libvirtd $USERNAME
```

### Python
The code has been tested on python 3.6.
The following python package are needed (pip):
```
pip3 install argparse pathlib bs4 pyyaml netifaces ipaddress lxml
```

### Linux kernel
The connections in the lab rely on linux bridges (http://man7.org/linux/man-pages/man8/bridge.8.html).
It is very extensive, but has integrated protection mechanisms that prevent certain types of frame to be forwarded. This is the case with LLDP, LACP or STP. This has been done for obvious, yet you might need them in a lab..
```diff
- /!\ Be careful no to mess with you production environment when dealing with these settings. /!\
```
In order to enable this specific frame forwarding behavior, you will need a modified kernel, with an altered bridge module.
You can find a procedure here: https://wiki.ubuntu.com/Kernel/BuildYourOwnKernel
Or download an already compiled kernel in the kernel folder.

## Create the topology

The topology creation relies on the yED desktop client.
For some reason the yED online client and yED desktop client does not generate the same graphml which confuses the parser.
I will improve this point in the future.

The yED desktop client can be found here (linux/windows/mac) : https://www.yworks.com/downloads#tools

There are two things to know
1- the node label will be the vm name
2- the description field needs to carry a statement "type=XXXX" where the XXXX is the image short name that you will find in the yaml file.

Once your topology is done, save it as .graphml file.

## Run the scripts

Current help menu:
```
usage: labgen.py [-h] [-g GRAPHML_FILE] [-v] [-p PROJECT] [-c] [-u] [-d]
                 [--admin-subnet ADMIN_SUBNET] [-w WEBFOLDER]

LABGen: creates vagrant files to build your own lab based on GRAPHML input

optional arguments:
  -h, --help            show this help message and exit
  -g GRAPHML_FILE, --graphml_file GRAPHML_FILE
                        Your input graph file that describe your network.
  -v, --verbose         increase output verbosity
  -p PROJECT, --project PROJECT
                        define a project name
  -c, --create          prepare all the lab environment
  -u, --up              start your lab
  -d, --destroy         destroy the lab
  --admin-subnet ADMIN_SUBNET
                        define your own admin subnet
  -w WEBFOLDER, --webfolder WEBFOLDER
                        output an html file into the selected folder (requires
                        a webserver and the absolute path)
```

### Create a lab
Use : 
* -c : launch the create function
* -p : (optional) define a project name (it will be the directory name) ; if not specified name is derived from graphml file.
* --admin-subnet : (optional) if you want a specific subnet for admin
* -g : input the graphml file
* -v : verbose
```
python3 labgen.py -v -c -p $PROJECT_NAME -g $GRAPHMLFILE.graphml
```

### Bring up a lab
Use : 
* -u : up, basically launch 'vagrant up'
* -p : project/directory where the Vagrantfile is. It is mandatory here.
* -v : verbose
```
python3 labgen.py -v -u -p $PROJECT_NAME
```

### Destroy the lab
Use : 
* -d : destroy, basically, launch 'vagrant destroy' and remove bridges and work folder
* -p : project/directory where the Vagrantfile is. It is mandatory here.
* -v : verbose
```
python3 labgen.py -v -d -p $PROJECT_NAME
```

## Lab web page
A lab guide and a host files are already produced in the lab folder by default.
In order to have labguide's webpage, you'll need to run a webserver.

You will also need to add the '-w /path/to/www/folder/' option to generate the HTML page. 

## Modified kernel
You'll be asked if you run a modified kernel. Such kernel should have a more permissive bridge module in order to let all the protocols go through (some of then are by default filtered: LACP, LLDP, STP...)
A number is required in that field: it is the bitmask value to apply (if you don't want any restrictions, put 65535.)

Be careful however on how you servers is connected, I'm sure you don't want to end up creating a loop in your network (those limitations on bridges are there for good reasons)

## Future work 
- halt function
- input validations
- support more input format: yed online graphml, draw.io, cumulus ptm
- console suppport
- move away for bridges

## Contact
Feel free to reach if this work can be extended / improved.
Constructive comments are welcomed.

=> There is much 
